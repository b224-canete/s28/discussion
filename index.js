//CRUD Operations

// Insert Document (Create)

/*
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA":"valueA",
				"fieldB":"valueB"
			});

		Insert Many Documents
			db.collectionName.insertMany([
				{
					"fieldA":"valueA",
					"fieldB":"valueB"
				},
				{
					"fieldA":"valueA",
					"fieldB":"valueB"
				}
			])

*/
// Upon execution on Robo3T make sure not to execute your several data times as it result to duplicates.


db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@gmail.com",
	"company": "none"
});

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@gmail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neil.armstrong@ymail.com",
			"department": "none"
		}
	]);

//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
	*/


db.courses.insertMany([
	{	
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": true
	}
]);

// Find Document (Read/Retrieve)

/*
	Syntax:
		db.collectionName.find() - this will retrieve all the documents from our DB.

		db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria.

		db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that matches our criteria.

		db.collectionName.findOne({}) - this will return the first document in our collection.

*/

db.users.find();


db.users.find({
	"firstName": "Jane"
});

db.users.find({
"firstName": "Neil",
"age": 82
});


// Updating documents (Update)
/*
	Syntax:
	Update one document
		db.collectionName.updateOne(
			{
				"criteria": "value"
			},
			{
				$set:{
					"fieldToBeUpdated": "updatedValue"
				}
			}
		);

		-updating the first matching document on our collection
		

		Multiple Many Documents
		db.collectionName.updateMany(
			{
				"criteria": "value"
			},
			{
				$set: {
					"fieldToBeUpdated": "updatedVAlue"
				}
			}
		)

*/


db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"department": "none"
});


// Updating One Document
db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set:{
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@gmail.com",
			"department": "Operations",
			"status": "active"
		}
	}
);

// Removing a field
db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset:{
			"status": "active"
		}
	}
);

// Updating Multiple Documents
db.users.updateMany(
	{
		"department": "none"
	},
	{
		$set: {
			"department": "HR"
		}
	}

);

// Update without criteria
db.users.updateOne(
	{},
	{
		$set: {
			"department": "Operations"
		}
	}
);


db.users.updateMany(
	{},
	{
		$rename: {
			"department": "dept"
		}
	}
);


/*
	Mini Activity:
		- Use updateOne() to change the isActive status of a course into false.
		-Use the updateMany() to set and add “enrollees” with a value of 10.
*/


db.courses.updateOne(
	{
		"name": "Javascript 101"
	},
	{
		$set:{
			"isActive": false
		}
	}
);

db.courses.updateMany(
	{},
	{
		$set:{
			"enrollees": 10
		}
	}
);


// Deleting Documents (Delete)

/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"criteria": "value"});

		DeletingMultiple Documents
			db.collectionName.deleteMany({"criteria":"value"});

*/


db.users.insertOne({
	"firstName": "Test"
});


db.users.deleteOne({
	"firstName": "Test"
});

db.users.deleteMany({
	"dept":"HR"
});

db.courses.deleteMany({});






